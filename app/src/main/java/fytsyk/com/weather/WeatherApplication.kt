package fytsyk.com.weather

import android.app.Application
import android.content.Context
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext

class WeatherApplication : Application() {
    val appModule = module {
        single { applicationContext as Context }
    }

    override fun onCreate() {
        super.onCreate()

        StandAloneContext.startKoin(listOf(appModule, main_module))
    }
}