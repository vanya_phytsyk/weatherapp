package fytsyk.com.weather.data.web.model
data class Main(
    val humidity: Int,
    val pressure: Double,
    val temp: Double,
    val temp_max: Double,
    val temp_min: Double
)