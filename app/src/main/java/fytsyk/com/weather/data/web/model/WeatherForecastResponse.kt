package fytsyk.com.weather.data.web.model

class WeatherForecastResponse(
    val city: CityResponse,
    val cnt: Int,
    val cod: String,
    val list: List<WeatherResponse>,
    val message: Double
)