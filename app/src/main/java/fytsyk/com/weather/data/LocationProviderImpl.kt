package fytsyk.com.weather.data

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import com.google.android.gms.location.*
import fytsyk.com.weather.domain.LocationData
import fytsyk.com.weather.domain.LocationProvider
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

class LocationProviderImpl(private val context: Context) : LocationProvider, LocationCallback() {
    private val mutableLocation = BehaviorSubject.create<LocationData>()

    private var fusedLocationClient: FusedLocationProviderClient? = null

    @SuppressLint("MissingPermission")
    override fun startLocationUpdates() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        fusedLocationClient?.lastLocation?.addOnSuccessListener {
            parseCurrentLocation(it)?.let { mutableLocation.onNext(it) }
        }
        val locationRequest = LocationRequest.create()?.apply {
            interval = 60 * 60 * 1000
            fastestInterval = 60 * 30 * 1000
            priority = LocationRequest.PRIORITY_LOW_POWER
        }
        fusedLocationClient?.requestLocationUpdates(null, this, null)
    }

    private fun parseCurrentLocation(location: Location?): LocationData? {
        return if (location != null) LocationData(location) else null
    }

    override fun stopLocationUpdates() {
        fusedLocationClient?.removeLocationUpdates(this)
    }

    override fun getCurrentLocation(): Observable<LocationData> {
        return mutableLocation
    }

    override fun onLocationResult(locationResult: LocationResult?) {
        val city = parseCurrentLocation(locationResult?.locations?.firstOrNull())
        city?.let { mutableLocation.onNext(city) }
    }
}