package fytsyk.com.weather.data.web.model

data class WeatherResponse(
    val clouds: Clouds,
    val dt: Long,
    val dt_txt: String,
    val main: Main,
    val rain: Rain,
    val sys: SysX,
    val weather: List<Weather>,
    val wind: Wind,
    val name: String
)