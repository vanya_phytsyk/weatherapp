package fytsyk.com.weather.data.web.model

data class Wind(
    val deg: Double,
    val speed: Double
)