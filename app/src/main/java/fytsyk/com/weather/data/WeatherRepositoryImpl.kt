package fytsyk.com.weather.data

import android.location.Location
import fytsyk.com.weather.data.web.WeatherService
import fytsyk.com.weather.domain.*
import io.reactivex.Observable
import java.text.SimpleDateFormat
import java.util.*

class WeatherRepositoryImpl(private val weatherService: WeatherService) : WeatherRepository {

    override fun getCurrentWeather(location: Location): Observable<CurrentWeather> {
        return weatherService.getWeather(location.latitude, location.longitude)
            .map {
                CurrentWeather(
                    City(it.name, it.sys.country),
                    (it.main.temp - 273.15).toInt(),
                    it.weather.first().id,
                    it.sys.sunset * 1000,
                    it.sys.sunrise * 1000,
                    it.main.pressure.toLong(),
                    it.wind.speed
                )
            }
    }

    override fun getWeatherForecast(location: Location): Observable<WeatherForecast> {
        return weatherService.getForecast(location.latitude, location.longitude)
            .map {
                val periods = it.list.map {
                    Period3HForecast(it.dt * 1000, it.weather.first().id, (it.main.temp - 273.15).toInt())
                }.groupBy {
                    Calendar.getInstance().apply { timeInMillis = it.hourPeriodStarts }.get(Calendar.DAY_OF_WEEK)
                }
                val days = periods.keys.map {
                    DayForecast(it, periods[it]!!)
                }
                WeatherForecast(days)
            }
    }
}