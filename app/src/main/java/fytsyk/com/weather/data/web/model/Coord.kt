package fytsyk.com.weather.data.web.model

data class Coord(
    val lat: Double,
    val lon: Double
)