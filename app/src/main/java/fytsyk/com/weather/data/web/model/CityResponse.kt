package fytsyk.com.weather.data.web.model

data class CityResponse(
    val coord: Coord,
    val country: String,
    val id: Int,
    val name: String
)