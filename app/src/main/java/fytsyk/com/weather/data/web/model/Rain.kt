package fytsyk.com.weather.data.web.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonQualifier

data class Rain(
    @Json(name = "3h")
    val three_hours: Double
)