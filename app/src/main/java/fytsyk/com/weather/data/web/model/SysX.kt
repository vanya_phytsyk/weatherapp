package fytsyk.com.weather.data.web.model

data class SysX(
    val country: String,
    val id: Int,
    val message: Double,
    val sunrise: Long,
    val sunset: Long,
    val type: Int
)