package fytsyk.com.weather.data.web.model

data class Weather(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)