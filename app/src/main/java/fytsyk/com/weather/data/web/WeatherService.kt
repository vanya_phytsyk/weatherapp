package fytsyk.com.weather.data.web

import fytsyk.com.weather.data.web.model.WeatherForecastResponse
import fytsyk.com.weather.data.web.model.WeatherResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("/data/2.5/forecast?appid=28c16f699f00431d3a4e85055b346aff")
    fun getForecast(@Query("lat") lat: Double, @Query("lon") lon: Double): Observable<WeatherForecastResponse>

    @GET("/data/2.5/weather?appid=28c16f699f00431d3a4e85055b346aff")
    fun getWeather(@Query("lat") lat: Double, @Query("lon") lon: Double): Observable<WeatherResponse>
}