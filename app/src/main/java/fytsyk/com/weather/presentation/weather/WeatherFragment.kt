package fytsyk.com.weather.presentation.weather

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import fytsyk.com.weather.databinding.FragmentWeatherBinding
import fytsyk.com.weather.presentation.MainActivity
import kotlinx.android.synthetic.main.bottom_sheet.view.*

class WeatherFragment : Fragment() {

    companion object {
        fun newInstance() = WeatherFragment()
    }

    private var dayForecastAdapter: DayForecastAdapter? = null

    private lateinit var viewModel: WeatherViewModel
    private lateinit var binding: fytsyk.com.weather.databinding.FragmentWeatherBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentWeatherBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.let { activity ->
            viewModel = ViewModelProviders.of(activity).get(WeatherViewModel::class.java)
            binding.lifecycleOwner = this
            binding.viewModel = viewModel
            viewModel.setup()
            binding.bottomSheet.forecast_list.layoutManager =
                LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            dayForecastAdapter = DayForecastAdapter()
            binding.bottomSheet.forecast_list.adapter = dayForecastAdapter

            binding.openMenu.setOnClickListener { (activity as? MainActivity)?.openDrawer() }
        }

        viewModel.weatherForecast.observe(this, Observer {
            dayForecastAdapter?.submitList(it.dayForecasts)
        })

    }

}
