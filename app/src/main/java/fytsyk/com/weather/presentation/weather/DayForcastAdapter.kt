package fytsyk.com.weather.presentation.weather

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fytsyk.com.weather.databinding.ListItemDayWeatherBinding
import fytsyk.com.weather.databinding.ListItemDayWeatherBindingImpl
import fytsyk.com.weather.domain.DayForecast

class DayForecastAdapter() :
    ListAdapter<DayForecast, DayForecastViewHolder>(ForecastDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayForecastViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemDayWeatherBindingImpl.inflate(inflater, parent, false)
        return DayForecastViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DayForecastViewHolder, position: Int) {
        val dayForecast = getItem(position)
        holder.itemBinding.dayForecast = dayForecast
        holder.itemBinding.executePendingBindings()
    }
}

class ForecastDiffCallback : DiffUtil.ItemCallback<DayForecast>() {
    override fun areItemsTheSame(oldItem: DayForecast, newItem: DayForecast): Boolean {
        return oldItem.dayNumber == newItem.dayNumber
    }

    override fun areContentsTheSame(oldItem: DayForecast, newItem: DayForecast): Boolean {
        return oldItem == newItem
    }

}

class DayForecastViewHolder( val itemBinding: ListItemDayWeatherBinding) :
    RecyclerView.ViewHolder(itemBinding.root) {

}