package fytsyk.com.weather.presentation

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.github.pwittchen.weathericonview.WeatherIconView
import fytsyk.com.weather.R
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("android:time")
fun setTime(textView: TextView, time: Long) {
    val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
    val dateFormat = timeFormat.format(Date(time))
    textView.text = dateFormat
}

@BindingAdapter("android:periodStartTime")
fun setPeriodStartTime(textView: TextView, time: Long) {
    val timeFormat = SimpleDateFormat("Ha", Locale.getDefault())
    val dateFormat = timeFormat.format(Date(time))
    textView.text = dateFormat
}

@BindingAdapter("android:dayNumber")
fun setWeekDayNumber(textView: TextView, dayNumber: Int) {
    val today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
    val day = if (today != dayNumber)
        textView.resources.getStringArray(R.array.week_days)[dayNumber - 1]
    else textView.resources.getString(R.string.today)
    textView.text = day
}

@BindingAdapter("android:weatherIcon")
fun setWeatherIcon(weatherIconView: WeatherIconView, iconId: Int) {
    if (iconId == 0) return
    val packageName = weatherIconView.context.packageName
    val iconReId = "wi_owm_$iconId"
    val resId = weatherIconView.resources.getIdentifier(iconReId, "string", packageName)
    weatherIconView.setIconResource(weatherIconView.resources.getString(resId))
}