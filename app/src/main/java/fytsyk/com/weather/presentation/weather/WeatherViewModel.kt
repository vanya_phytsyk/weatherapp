package fytsyk.com.weather.presentation.weather

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import fytsyk.com.weather.domain.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class WeatherViewModel : ViewModel(), KoinComponent {
    private var initialized: Boolean = false

    private val locationProvider: LocationProvider by inject()
    private val weatherRepository: WeatherRepository by inject()

    private val compositeSubscriptions = CompositeDisposable()

    val currentWeather = MutableLiveData<CurrentWeather>()
    val weatherForecast = MutableLiveData<WeatherForecast>()

    override fun onCleared() {
        compositeSubscriptions.dispose()
    }

    fun setup() {
        if (initialized) return
        initialized = true

        subscribeToLocationUpdates()
    }

    private fun subscribeToLocationUpdates() {
        compositeSubscriptions += locationProvider.getCurrentLocation()
            .subscribeOn(Schedulers.io())
            .subscribe {
            loadCurrentWeather(it)
            loadWeatherForecast(it)
        }
    }

    private fun loadWeatherForecast(locationData: LocationData) {
        compositeSubscriptions += weatherRepository.getWeatherForecast(locationData.location)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                weatherForecast.postValue(it)
            }, { it.printStackTrace() })
    }

    private fun loadCurrentWeather(locationData: LocationData) {
        compositeSubscriptions += weatherRepository.getCurrentWeather(locationData.location)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                currentWeather.postValue(it)
            }, { it.printStackTrace() })
    }


}
