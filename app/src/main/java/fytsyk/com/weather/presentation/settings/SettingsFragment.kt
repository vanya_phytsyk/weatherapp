package fytsyk.com.weather.presentation.settings

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fytsyk.com.weather.databinding.FragmentSettingsBinding

import fytsyk.com.weather.databinding.FragmentWeatherBinding
import fytsyk.com.weather.presentation.MainActivity
import fytsyk.com.weather.presentation.weather.DayForecastAdapter
import kotlinx.android.synthetic.main.bottom_sheet.view.*

class SettingsFragment : Fragment() {

    companion object {
        fun newInstance() = SettingsFragment()
    }


    private lateinit var viewModel: SettingsViewModel
    private lateinit var binding: fytsyk.com.weather.databinding.FragmentSettingsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SettingsViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        viewModel.setup()

        activity?.let { activity ->

        }

        binding.openMenu.setOnClickListener{
            (activity as? MainActivity)?.openDrawer()
        }



    }

}
