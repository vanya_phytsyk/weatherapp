package fytsyk.com.weather.presentation

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat
import fytsyk.com.weather.R
import fytsyk.com.weather.domain.LocationProvider
import fytsyk.com.weather.presentation.settings.SettingsFragment
import fytsyk.com.weather.presentation.weather.WeatherFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class MainActivity : AppCompatActivity(), KoinComponent {

    private val locationProvider: LocationProvider by inject()

    private val PERMISSION_REQUEST_CODE = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showWeatherFragment()
        item_weather.isChecked = true
        item_settings.setOnClickListener {
            closeDrawer()
            showSettingsFragment()
        }
        item_weather.setOnClickListener {
            closeDrawer()
            showWeatherFragment()
        }
    }

    private fun showSettingsFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.container, SettingsFragment.newInstance()).commit()
    }

    private fun showWeatherFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.container, WeatherFragment.newInstance()).commit()
    }

    override fun onResume() {
        super.onResume()
        if (isLocationPermissionsGranted())
            locationProvider.startLocationUpdates()
        else
            requestLocationPermissions()
    }

    override fun onPause() {
        super.onPause()
        locationProvider.stopLocationUpdates()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer()
        } else {
            super.onBackPressed()
        }
    }

    fun openDrawer() {
        drawer_layout.openDrawer(GravityCompat.START)
    }

    fun closeDrawer() {
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    private fun requestLocationPermissions() {
        requestPermissions(
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_REQUEST_CODE
        )
    }

    private fun isLocationPermissionsGranted() =
        ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when {
            requestCode == PERMISSION_REQUEST_CODE
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED -> locationProvider.startLocationUpdates()
        }
    }
}
