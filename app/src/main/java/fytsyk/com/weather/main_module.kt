package fytsyk.com.weather

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.squareup.moshi.Moshi
import fytsyk.com.weather.data.LocationProviderImpl
import fytsyk.com.weather.data.WeatherRepositoryImpl
import fytsyk.com.weather.data.web.WeatherService
import fytsyk.com.weather.domain.LocationProvider
import fytsyk.com.weather.domain.WeatherRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

const val API_URL = "https://api.openweathermap.org/"

val main_module = module {
    single { LocationProviderImpl(get()) as LocationProvider }
    single { weatherService() }
    single { WeatherRepositoryImpl(get()) as WeatherRepository }
}

fun weatherService(): WeatherService {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    val client = OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()
    val authRetrofit = Retrofit
        .Builder()
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(API_URL)
        .build()

    return authRetrofit.create<WeatherService>(WeatherService::class.java)
}