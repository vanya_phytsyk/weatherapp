package fytsyk.com.weather.domain

import android.location.Location

data class LocationData(val location: Location) {
}