package fytsyk.com.weather.domain

import java.util.*

class CurrentWeather(
    val city: City,
    val temperature: Int,
    val weatherId: Int,
    val sunset: Long,
    val sunrise: Long,
    val preassure: Long,
    val wind: Double
) {
}