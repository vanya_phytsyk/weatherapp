package fytsyk.com.weather.domain

import android.location.Location
import io.reactivex.Observable

interface WeatherRepository {
    fun getCurrentWeather(location: Location) : Observable<CurrentWeather>
    fun getWeatherForecast(location: Location) : Observable<WeatherForecast>
}