package fytsyk.com.weather.domain

class WeatherForecast(val dayForecasts: List<DayForecast>)

data class DayForecast(val dayNumber: Int,val  periods: List<Period3HForecast>)

data class Period3HForecast(val hourPeriodStarts: Long, val weatherId: Int, val temperature: Int)