package fytsyk.com.weather.domain

import io.reactivex.Observable


interface LocationProvider {
    fun getCurrentLocation() : Observable<LocationData>
    fun stopLocationUpdates()
    fun startLocationUpdates()
}